From rhawking@mail.cern.ch Tue Feb 15 20:54:36 2005
Date: Tue, 15 Feb 2005 09:19:59 +0100 (CET)
From: Richard Hawkings <rhawking@mail.cern.ch>
To: Sven A. Schmidt <sas@abstracture.de>
Cc: Andrea Valassi <Andrea.Valassi@cern.ch>, RD Schaffer <R.D.Schaffer@cern.ch>,
     Torre Wenaus <Torre.Wenaus@cern.ch>, Antoine Perus <perus@lal.in2p3.fr>
Subject: Re: More COOL API comments

Dear All,

Following on from our discussion on the interface semantics yesterday, RD 
and I thought it might be possible to overload operator[] in IObject.h.

This would allow you to do something like

x=object<float>["X"];
i=object<int>["I"];

On the other hand, maybe the array syntax would be better used in the 
store methods, to allow

payload1["I"]=1;


but I'm not sure how the templating would work. Thinking about it again 
this morning, its less obvious to me now, so maybe this is not such a good 
idea.... any comments?

cheers,
Richard.





