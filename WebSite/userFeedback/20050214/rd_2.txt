From R.D.Schaffer@cern.ch Tue Feb 15 20:39:26 2005
Date: Mon, 14 Feb 2005 15:44:57 +0100
From: RD Schaffer <R.D.Schaffer@cern.ch>
To: Sven A. Schmidt <sas@abstracture.de>
Cc: Torre Wenaus <Torre.Wenaus@cern.ch>,
     Andrea Valassi <Andrea.Valassi@cern.ch>,
     Antoine Perus <perus@lal.in2p3.fr>,
     Richard Hawkings <rhawking@mail.cern.ch>
Subject: Re: feedback on COOL interfaces

Hi Sven,


On 14 Feb, 2005, at 13:34, Sven A. Schmidt wrote:

> Hi RD,
>
> I'll reply with some comments even though we'll talk about it later as 
> well, but I guess it's a bit easier in writing.
>
> On 14.02.2005, at 12:34, RD Schaffer wrote:
>
>>   - don't see any transaction management
>>     => Should require all actions to be embedded inside
>>     startRead/commit, or startUpdate/commit and allow abort.
>>     One should create dbs within a transaction and manipulate folders
>>     within a transaction.
>
> The idea was not to expose the transaction handling to the user. All 
> methods that a user can call represent one transaction and are rolled 
> back should any exception occur. The intention is for the user to set 
> up all he wants to do and then 'commit' it in a call to the API, like
>
> db = dbSvc.openDatabase(...)    // obtain connection, this stays open 
> while db 'lives'
> folder = db.createFolder(...)   // transaction started and ended
> // set up data
> folder.setupStorageBuffer()     // no transaction, just a client cache 
> set up
> folder.storeObject( data )      // cache filling
> folder.flushStorageBuffer()     // now commit, transaction started and 
> ended
>
> Does that cover the transaction handling you would like to see?

Do we know today which transaction model is the one do adopt? I.e. I 
assume that without an explicit start/commit/abort each "create" or 
"store" must be a full commit cycle. Is this the optimal, or does one 
want to allow multiple actions to be inside a single transaction, with 
perhaps a certain amount of programming logic to decide before 
committing or aborting? This is not clear to me now.

Another point is connections. If I look at the Athena interface for 
event processing (IOVDbSvc today), the scenario that we currently have 
is:

1) at job init, get "handles" to the different folders that have been 
requested in job config (jobOpts)
2) at first use of each handle, we would like to open a connection and 
start a read transaction and read in the data for each folder
3) when we "know" we are finished with the first use of each folder, 
(e.g. at the end of the first event) we would like to "close the 
connection" to the various folders used, but we want to hold onto the 
folder "handles" so that they can be (possibly) used later
4) as objects go out of validity, we would like to go through 2) and 3) 
on a folder by folder basis
5) new folders which are needed may be discovered during processing

It looks like from your example above that  in order to close the 
connection one must drop all folderPtrs and close the database (drop 
the ptr?). Then one would have to reaccess each folder to get its ptr 
again at each use. Why not hold onto them?

>
>> class IDatabaseSvc
>>
>>     /// Drop an existing database
>>
>>     What does "Drop" mean?  Drop a connection to? Delete the database?
>>
>>     Recommend create/delete  but not drop. (I have heard of "dropping 
>> connections" but this is
>>     the "system" that does this and not a client.)
>
> It means drop as in 'drop database'. I agree 'delete' would be more 
> obvious.

I should have known that "standard" sql has "drop" mean delete. I would 
prefer delete, but could live with the standard.

>
>>     For folder creation: I agree with change to have just
>>     "description" and not both "description" and "attributes"
>
> Hm, where is this attributes argument? Are you referring to

I was refering to the old interface that had two string values.

>
> IDatabase::createFolder (const std::string &fullPath, const 
> pool::AttributeListSpecification &spec, const std::string 
> &description="", const FolderVersioning::Mode 
> mode=FolderVersioning::ONLINE, const bool createParents=false)=0 ?
>
> The spec is the folder schema if that is what you mean.
>
>>     What are DatabaseAttributes? Doesn't an attribute spec apply only
>>     to a folder?
>>
>>     (I see the reply to Richard:  "This set of attributes can for 
>> example be used to change the names of the management tables of a new 
>> instance."  but I am not sure what this means. I think I still need 
>> more info to see the usefulness of this.)
>
> Ah, I see this is from
>
> IDatabaseSvc::createDatabase (const DatabaseId &dbId, const 
> pool::AttributeList &dbAttr)=0
>
> To clarify, the current implementation gets the following two 
> attributes from dbAttr and uses them when you create a new database 
> instance from scratch:
>
> key                   example value
> DEFAULT_TABLE_PREFIX  COOLTEST_
> FOLDER_TABLE_NAME     COOLTEST_FOLDERS
>
> So you can in principle configure your COOL installation to some 
> extent here.

OK, I understand now.  Looks like the possible attributes need to be 
documented somewhere.

>
>>     For method listFolders:
>>
>>       /// Return the list of existing folders (ordered by creation 
>> time asc/desc)
>>       virtual const std::vector<std::string>
>>       listFolders( const bool ascending = true ) = 0;
>>
>>       it is better to provide an "output argument" rather than to
>>       return a vector due to multiple copying.
>
> We specifically opted for
>
>  out method( in, ..., in );
>
> wherever there are no performance issues to be expected from copying 
> to avoid the typical 'who owns it, who destroys it' problems. In case 
> performance should cause trouble considering the number of folders one 
> can manage we could change it to
>
> vector<string>::const_iterator beginFolders( const bool ascending = 
> true )
> vector<string>::const_iterator endFolders()
>
> and keep the list inside the IDatabase object. But this is a bit 
> awkward, too.
>
> Where we expect performance issues from copying, we added shared_ptrs 
> to be able to stick to the pattern:
>
>   boost::shared_ptr<out> method( in, ..., in )
>
> but in this case
>
>   boost::shared_ptr< vector<string> > listFolders()
>
> looks like overkill :).

Whatever you like, but I don't think that you should return from a 
method anything but ptrs, simple types, and refs. Large objects just 
shouldn't be return values, which effectively excludes collections.

>
>> class IObject:
>>
>>     What does the following method mean?:
>>
>>     /// Has the object been stored into the database?
>>     virtual const bool isStored() const = 0;
>
> This relates to the system objects I mentioned in my reply to Richard 
> and shouldn't really be exposed. isStored() is true for 'user inserted 
> objects' and false for 'system created objects'.
>
>

      thanks, RD

Email:    R.D.Schaffer@cern.ch
Address:  LAL BAT 200        tel(Orsay): 33-1 64 46 8378
                   BP 34                     tel(cern) : 41-22 76 71267
                  91898 ORSAY
                  France

