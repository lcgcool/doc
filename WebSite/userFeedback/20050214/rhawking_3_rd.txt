From R.D.Schaffer@cern.ch Mon Feb 14 14:34:56 2005
Date: Mon, 14 Feb 2005 12:15:28 +0100
From: RD Schaffer <R.D.Schaffer@cern.ch>
To: Richard Hawkings <rhawking@mail.cern.ch>
Cc: Torre Wenaus <Torre.Wenaus@cern.ch>,
     Andrea Valassi <Andrea.Valassi@cern.ch>,
     Sven A. Schmidt <sas@abstracture.de>, Antoine Perus <perus@lal.in2p3.fr>
Subject: Re: feedback on COOL interfaces

Hi Richard,


On 14 Feb, 2005, at 12:06, Richard Hawkings wrote:

> Dear All,
>
> A couple more comments on the COOL inferfaces...
>
>  - as mentioned by Karl, it would be useful if there was a 'modify
> description' method in the IFolder class - we have sometimes had to 
> change
> a folder's meta data after it was created in 'real life'.

I agree here.


>
> Mor generally, the syntax for setting and retrieving data members is 
> quite
> 'ugly', i.e. the
>
> payload1["I"].setValue<int>(7)
>
> and
>
> xxx=payloadValue<int>("I")

For the syntax discussion (above and below), I think that what you want 
is to use pool object relational where you have a simple class 
specified in C++ and this is more or less what the user will see. This 
should still have schema evolution possibilities because it inherits 
the relational extensibility. I am not sure you will get much further 
with the simplification of AttributeList.

               see you, RD


>
> This has some plusses and minuses:
>  - on the plus side, it provides for a minimum of schema evolution - 
> you
> can easily add columns to tables as the existing columns are always
> identified dynamically by name (presumably you get exceptions if you
> try to access non-existent columns?)
>
>  - on the minus side, you have to do a lot of explicit typing... you 
> might
> have your transient data as
>
> struct mydata {
>   int i;
>   string s;
>   float x;
> }
>
> then want to do:
>
>   mydata instance;
>   // set up mydata
>   // ...
>   // store it
>   folder->store(0,10,mydata,1);
>
>   //read back
>   instance=folder->findObject(1,mydata);
>
> With the current interface, this requires a lot of
>
>    mydata.i=payloadValue<int>("I");
>    mydata.s=payloadValue<string>("S");
>
> etc... I don't know if its possibe/desirable to wrap/template some of 
> this
> so its easier for users? This was hinted at during a presentation from
> Sven at an ATLAS database meeting, where it was said that the level of
> interface we see now might not be what the users see.
> I'm not sure what the correct answer is - for discussion...
>
> cheers,
> Richard.
>
>
>
>
Email:    R.D.Schaffer@cern.ch
Address:  LAL BAT 200        tel(Orsay): 33-1 64 46 8378
                   BP 34                     tel(cern) : 41-22 76 71267
                  91898 ORSAY
                  France

