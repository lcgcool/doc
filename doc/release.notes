Conditions Database project: Documentation (doc package) release notes

==============================================================================
!2008.06.10 - COOL_2_5_0

Documentation for COOL_2_5_0.

Update (rewrite almost completely) performance.html to mention hints.

==============================================================================
!2008.06.04 - COOL_2_4_0a

Documentation for COOL_2_4_0a.

==============================================================================
!2008.02.28 - COOL_2_4_0

Documentation for COOL_2_4_0.

Adapt platformSupport after the move of logfiles 
from cool/config/qmtest/logs to cool/logs/qmtest in CVS.

==============================================================================
!2008.02.21 - COOL_2_3_1

Documentation for COOL_2_3_1.

A posteriori, adapt platformSupport after the move of logfiles 
from cool/config/qmtest/logs to cool/logs/qmtest in CVS:
  cvs rtag -r 1.55 -b COOL_2_3-branch cool/doc/WebSite/platformSupport.html 
Retag the new version as COOL_2_3_1, reinstall it on AFS and resync.

Documentation is presently on /afs/.cern.ch because AFS resync is broken.

==============================================================================
!2008.01.21 - COOL_2_3_0

Documentation for COOL_2_3_0.

Remove all SCRAM-related fragments (previously commented out).

==============================================================================
!2007.11.13 - COOL_2_2_2

Documentation for COOL_2_2_2.

The howToRelease procedure is much simpler as the release is now built by SPI.

==============================================================================
!2007.11.08

Remove (comment out) all links to the bash test logfiles in the 
platform support page: only keep the links to the QMTEST logfiles.

==============================================================================
!2007.11.01

Removed old minutes from CVS HEAD and moved them to the protected Web site.

==============================================================================
!2007.10.11 - COOL_2_2_1

Documentation for COOL_2_2_1.

==============================================================================
!2007.07.13 - COOL_2_2_0

Documentation for COOL_2_2_0.

==============================================================================
!2007.04.16 - COOL_2_1_1

Documentation for COOL_2_1_1.

==============================================================================
!2007.03.24 - COOL_2_1_0

Documentation for COOL_2_1_0.

==============================================================================
!2007.01.31 - COOL_2_0_0

Documentation for COOL_2_0_0.

==============================================================================
!2006.10.17 - COOL_1_3_4

Documentation for COOL_1_3_4.

==============================================================================
!2006.10.17 - COOL_1_3_3c

Documentation for COOL_1_3_3c.

==============================================================================
!2006.10.17 - COOL_1_3_3b

Documentation for COOL_1_3_3b.

==============================================================================
!2006.10.12 - Marco

Updated the WINE tutorial to 0.9.22.

==============================================================================
!2006.09.29 - COOL_1_3_3a

Documentation for COOL_1_3_3a.

==============================================================================
!2006.09.11 - COOL_1_3_3

Documentation for COOL_1_3_3.

Added Kristoffer's performance reports to the released documentation.
Did not include it (yet?) in CVS as it involves a huge number of files.
Adding the report to the released documentation means that this will remain 
for reference in the future even if the performance report tool is modified; 
improved reports for COOL_1_3_3 can always be produced a posteriori.

==============================================================================
!2006.07.12 - COOL_1_3_2c

Documentation for COOL_1_3_2c.

==============================================================================
!2006.06.19 - COOL_1_3_2b

Documentation for COOL_1_3_2b.

==============================================================================
!2006.05.16 - COOL_1_3_2

Documentation for COOL_1_3_2a.
Added platformSupport.html.

==============================================================================
!2006.05.12 - COOL_1_3_2

Documentation for COOL_1_3_2.

==============================================================================
!2006.04.25 - COOL_1_3_1

Documentation for COOL_1_3_1.

==============================================================================
!2006.04.06 - COOL_1_3_0

Documentation for COOL_1_3_0.

==============================================================================
!2006.03.08 - COOL_1_2_9

Documentation for COOL_1_2_9.

==============================================================================
!2006.01.27 - COOL_1_2_8

Documentation for COOL_1_2_8.

==============================================================================
!2006.01.16 - COOL_1_2_7

Documentation for COOL_1_2_7.

==============================================================================
!2005.11.15 - COOL_1_2_6

Documentation for COOL_1_2_6.

==============================================================================
!2005.10.19 - COOL_1_2_5

Documentation for COOL_1_2_5.

==============================================================================
!2005.09.30

A few improvements to the user guide.

==============================================================================
!2005.09.27 - COOL_1_2_4

Documentation for COOL_1_2_4.

==============================================================================
!2005.08.29 - COOL_1_2_3

Documentation for COOL_1_2_3.

==============================================================================
!2005.08.19

First public draft of the COOL User Guide.

==============================================================================
!2005.08.04

Added doc/doxygen subdirectory with tools and dox sources for the 
SPI-generated doxygen documentation and the doxygen-based user guide.

Added doc/doc subdirectory with release.notes moved from doc/WebSite/doc.
Removed doc/WebSite/doc subdirectory at filesystem level to avoid conflicts.

Added doc/WebSite/contrib subdirectory for the documentation of the
contrib packages (so far: Cool2Root, COOL_IO, StandaloneCool).

==============================================================================
!2005.07.27 - COOL_1_2_2

Documentation for COOL_1_2_2.

==============================================================================
!2005.07.20 - COOL_1_2_1

Documentation for COOL_1_2_1.

==============================================================================
!2005.06.28 - COOL_1_2_0

Documentation for COOL_1_2_0.

==============================================================================
!2005.05.31 - COOL_1_1_0

Documentation for COOL_1_1_0.

==============================================================================
!2005.05.10 - COOL_1_0_2

Documentation for COOL_1_0_2.

Migration to the cool.cvs.cern.ch CVS repository.

==============================================================================
!2005.04.28 - COOL_1_0_1

Documentation for COOL_1_0_1.

==============================================================================
!2004.07.22 - CONDDB-DOC_0_4_0

Documentation for CONDDB_0_2_0.

Add instructions for rh73_gcc323 and cel3-i386_gcc323, 
remove those for rh73_gcc2952.

Give instructions only for the tcsh shell.

Assume a snapshot documentation directory is installed.

==============================================================================
!2004.06.16 

Removed the "current" logical link from the WWW directory.
Replaced it by a "snapshot" of the CVS HEAD at any given time. 
This makes it possible to update the Web page of the project (to add minutes
of meetings, for instance) without waiting for a new tagged software release.

==============================================================================
!2004.05.06 - CONDDB-DOC_0_3_0

Documentation for CONDDB_0_1_1.

Change default SCRAM platform from rh73_gcc2952 to rh73_gcc32.

Work with the project installed in the SCRAM lookup database.

==============================================================================
!2004.04.22 - CONDDB-DOC_0_2_0

Documentation for CONDDB_0_1_0.

Add documentation for CondDBMySQL (for both SCRAM and config/make).

==============================================================================
!2004.03.25 - CONDDB-DOC_0_1_0

Documentation for CONDDB_0_1_0-pre1.

Migration to new CVS repository.

==============================================================================
!2004.03.23 - CONDDB-DOC_0_0_2

Documentation for CONDDB_0_0_0-pre3.

No significant changes in documentation.

==============================================================================
!2004.02.23 - CONDDB-DOC_0_0_1

Documentation for CONDDB_0_0_0-pre2.

Keep the documentation for all releases in separate directories. 

==============================================================================
!2004.01.28 - CONDDB-DOC_0_0_0

Documentation for first internal release CONDDB_0_0_0-pre1.

==============================================================================
