#! /bin/sh

PROJTAG=COOL_1_2_2
PROJ=COOL
PROJLOW=cool

CVSPROJ=COOL
CVSTOP=cool

PROJDIRTAG=$PROJTAG

WORKTOP=/build/lcgspi/doxygen
STARTDIR=~lcgspi/scripts/doxygen

echo "=============== GENERATE DOXYGEN BEGIN"
source $STARTDIR/generate-doxygen.sh
echo "=============== GENERATE DOXYGEN END"
