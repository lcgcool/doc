#! /bin/sh

PROJTAG=HEAD
PROJDIRTAG=COOL_snapshot

PROJ=COOL
PROJLOW=cool

CVSPROJ=COOL
CVSTOP=cool

WORKTOP=/build/lcgspi/doxygen
STARTDIR=~lcgspi/scripts/doxygen


echo "=============== GENERATE DOXYGEN BEGIN"
source $STARTDIR/generate-doxygen.sh
echo "=============== GENERATE DOXYGEN END"
