#! /bin/bash

PLATF=slc3_gcc323

#-------------------

PATH=/usr/local/gcc-alt-3.2.3/bin:/afs/cern.ch/sw/lcg/app/spi/tools/0.6.1/scripts:/afs/cern.ch/sw/lcg/app/spi/scram:/opt/SUNWspro/bin/:/afs/cern.ch/user/l/lcgspi/scripts/doxygen:/usr/sue/bin:/usr/bin:/bin:/usr/bin/X11:/usr/local/bin:/usr/local/bin/X11:/cern/pro/bin:.:/afs/cern.ch/group/jp/scripts:/afs/cern.ch/sw/lcg/external/mysql/4.0.18/${PLATF}/bin
export PATH

LD_LIBRARY_PATH=/usr/local/gcc-alt-3.2.3/lib:/afs/cern.ch/sw/lcg/external/mysql/4.0.18/${PLATF}/lib:/usr/local/lib:/usr/lib:/afs/cern.ch/sw/lcg/external/graphviz/1.9/${PLATF}/lib/graphviz/
export LD_LIBRARY_PATH

#-------------------
rm -rf ${WORKTOP}/$PROJDIRTAG
mkdir -p ${WORKTOP}/$PROJDIRTAG/checkout
cd ${WORKTOP}/$PROJDIRTAG/checkout


cvs -Q -d :pserver:anonymous@${CVSPROJ}.cvs.cern.ch:/cvs/${CVSPROJ} co -r $PROJTAG ${CVSTOP} 


rm -rf ${WORKTOP}/$PROJDIRTAG/$PROJLOW
mv ${WORKTOP}/$PROJDIRTAG/checkout/${CVSTOP} ${WORKTOP}/$PROJDIRTAG/$PROJLOW


mkdir -p  ${WORKTOP}/$PROJDIRTAG/$PROJLOW/config/doxygen

if [[ ! -e ${WORKTOP}/$PROJDIRTAG/$PROJLOW/config/doxygen/Doxyfile_${PROJ}.cfg ]]; then
    echo "---------- Doxyfile_${PROJ}.cfg does not exist"
else
   cp -f ${WORKTOP}/$PROJDIRTAG/$PROJLOW/config/doxygen/Doxyfile_${PROJ}.cfg  ${STARTDIR}/doxygen_${PROJ}_config

    echo "---------- Doxyfile_${PROJ}.cfg exists"
fi

cp -rf ${STARTDIR}/doxygen_${PROJ}_config/* ${WORKTOP}/$PROJDIRTAG/$PROJLOW/config/doxygen

cd ${WORKTOP}/$PROJDIRTAG/$PROJLOW/config/doxygen

mkdir -p ../../doc/doxygen

/afs/cern.ch/sw/lcg/external/doxygen/1.4.1/${PLATF}/bin/doxygen ./Doxyfile_${PROJ}.cfg

rm -rf /afs/cern.ch/sw/lcg/app/spi/doc/doxygen/output/$PROJ/$PROJDIRTAG/doxygen


mkdir -p /afs/cern.ch/sw/lcg/app/spi/doc/doxygen/output/$PROJ/$PROJDIRTAG/doxygen

 python ~/scripts/repl_string.py ${WORKTOP}/$PROJDIRTAG/$PROJLOW/doc/doxygen/html/reference_tags.xml ${WORKTOP}/$PROJDIRTAG/$PROJLOW/ /afs/cern.ch/sw/lcg/app/releases/$PROJ/$PROJDIRTAG/src/ 


echo cp -rf ${WORKTOP}/$PROJDIRTAG/$PROJLOW/doc/doxygen/html /afs/cern.ch/sw/lcg/app/spi/doc/doxygen/output/$PROJ/$PROJDIRTAG/doxygen/
cp -rf ${WORKTOP}/$PROJDIRTAG/$PROJLOW/doc/doxygen/html /afs/cern.ch/sw/lcg/app/spi/doc/doxygen/output/$PROJ/$PROJDIRTAG/doxygen/


echo mv /afs/cern.ch/sw/lcg/app/spi/doc/doxygen/output/$PROJ/$PROJDIRTAG/doxygen/html/reference_tags.xml /afs/cern.ch/sw/lcg/app/spi/doc/doxygen/output/$PROJ/$PROJDIRTAG/doxygen/html/${PROJDIRTAG}_reference_tags.xml
mv /afs/cern.ch/sw/lcg/app/spi/doc/doxygen/output/$PROJ/$PROJDIRTAG/doxygen/html/reference_tags.xml /afs/cern.ch/sw/lcg/app/spi/doc/doxygen/output/$PROJ/$PROJDIRTAG/doxygen/html/${PROJDIRTAG}_reference_tags.xml

ln -s /afs/cern.ch/sw/lcg/app/spi/doc/doxygen/output/$PROJ/$PROJDIRTAG/doxygen/html/${PROJDIRTAG}_reference_tags.xml  /afs/cern.ch/sw/lcg/app/spi/doc/doxygen/output/$PROJ/$PROJDIRTAG/doxygen/html/${PROJ}_reference_tags.xml

lynx -dump -nolist http://lcgapp.cern.ch/doxygen/$PROJ/$PROJDIRTAG/doxygen/html/

echo rm -rf ${WORKTOP}/$PROJDIRTAG
rm -rf ${WORKTOP}/$PROJDIRTAG


echo emacs /afs/cern.ch/sw/lcg/app/spi/doc/doxygen/output/index.html
