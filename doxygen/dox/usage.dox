namespace cool {

/*! \page usage Getting Started

The getting started section intends to give a quick overview of the API features and present common use cases. Do not rely on them as sole documentation but use them as a starting point.

All examples presented in this section are implemented as actual unit tests in the RelationalFolderTest package. Please refer to this test for more details.

The examples cover the following use cases:

- \ref dbhandle
- \ref sv_rw
- \ref sv_bulk_rw
- \ref mv_rw
- \ref tag_w
- \ref tag_r



\section dbhandle Obtaining a Database Handle

Specify a connect string:

\code
DatabaseId connectString =
  "oracle://devdb9;schema=aSchema;user=aUser;password=xxx;dbname=COOLTEST";
\endcode

This is the only place where a technology is specified.

Then use an "open or create" pattern to obtain a database handle:

\code
IDatabaseSvc& dbSvc = RalDatabaseSvcFactory::databaseService();
IDatabasePtr db;
try {
  db = dbSvc.openDatabase( connectString );
} catch ( RelationalDatabaseDoesNotExist & e ) {
  db = dbSvc.createDatabase( connectString );
}
\endcode

Alternatively use an "drop and recreate" pattern to obtain a database handle:

\code
IDatabaseSvc& dbSvc = RalDatabaseSvcFactory::databaseService();
dbSvc.dropDatabase( connectString );
IDatabasePtr db = dbSvc.createDatabase( connectString );
\endcode




\section sv_rw Single Version, Single Object Storage and Retrieval

The first step to store an object is to set up the data "schema" or specification:

\code
AttributeListSpecification payloadSpec1;
payloadSpec1.push_back("I","int");
payloadSpec1.push_back("S","string");
payloadSpec1.push_back("X","float");
\endcode

The following examples use a method \c dummyPayload to create unique dummy data from the specification. It illustrates how an \c AttributeList is templated from the specification and how to set its values:

\code
/// Creates a dummy payload AttributeList for a given index
AttributeList dummyPayload( int index ) {
  AttributeList payload( payloadSpec1 );
  payload["I"].setValue<int>( index );
  stringstream s;
  s << "Object " << index;
  payload["S"].setValue<std::string>( s.str() );
  payload["X"].setValue<float>( (float)(index/1000.) );
  return payload;
}
\endcode

Refer to the \c pool::AttributeList documentation for more details, especially note the constructor documentation and the notes regarding the specification's lifetime: Make sure an \c AttributeListSpecification outlives an \c \c AttributeList that is created from it <em> unless </em> you have created the \c AttributeList from a \c boost::shared_ptr wrapped specification.

After the specification is set up, it is used to create a folder:

\code
IFolderPtr folder = db->createFolder( "/myfolder", payloadSpec1 );
\endcode

The \c createFolder method returns a folder pointer which is subsequently used to store objects:

\code
ValidityKey since( 0 ), until( 2 );
folder->storeObject( since, until, dummyPayload( 1 ) );
since = 2, until = 4;
folder->storeObject( since, until, dummyPayload( 2 ) );
\endcode

Note that IOVs <em> must not </em> overlap -- unless \c until equals \c ValidityKeyMax, in which case the IOV is interpreted as "valid until updated":

\code
folder->storeObject( 4, ValidityKeyMax, dummyPayload( 3 ) );
\endcode

Object (IObjectPtr) retrieval is also achieved through a folder pointer. The cppunit macros in the following code illustrate the values one can retrieve from an \c IObjectPtr:

\code
ValidityKey pointInTime( 1 );
IObjectPtr obj1 = folder->findObject( pointInTime );
CPPUNIT_ASSERT_MESSAGE( "first object",
                        dummyPayload( 1 ) == obj1->payload() );
      
pointInTime = 3;
IObjectPtr obj2 = folder->findObject( pointInTime );
CPPUNIT_ASSERT_MESSAGE( "second object",
                        dummyPayload( 2 ) == obj2->payload() );
\endcode


\section sv_bulk_rw Single Version, Bulk Storage and Retrieval

The bulk storage is very similar to the single object storage, the only difference is the call to \c setupStorageBuffer to signal bulk storage to the API and, after calling \c storeObject for all objects, the call to \c flushStorageBuffer to perform the actual database transaction:

\code
IFolderPtr folder = db->createFolder( "/myfolder", payloadSpec1 );

folder->setupStorageBuffer();
unsigned long nObjs = 100;
for ( unsigned long i = 0; i < nObjs; ++i ) {
  folder->storeObject( i, ValidityKeyMax, dummyPayload( i ) );
}
folder->flushStorageBuffer();
\endcode

Bulk retrieval on the other hand is achieved via a call to the \c browseObjects method of \c IFolder. Again, the cppunit macros illustrate the return values:

\code
IObjectIteratorPtr objs = folder->browseObjects( ValidityKeyMin,
                                                 ValidityKeyMax );

for ( unsigned long i = 0; i < objs->size(); ++i ) {
  IObjectPtr obj = objs->next();
  stringstream s; s << "object " << i << " ";
  CPPUNIT_ASSERT_MESSAGE( ( s.str() + "payload" ).c_str(), 
                          dummyPayload( i ) == obj->payload() );
  CPPUNIT_ASSERT_EQUAL_MESSAGE( ( s.str() + "since" ).c_str(), 
                                (ValidityKey)i, obj->since() );
  // last object's until extends to ValidityKeyMax
  if ( i < nObjs-1 ) {
    CPPUNIT_ASSERT_EQUAL_MESSAGE( ( s.str() + "until" ).c_str(), 
                                  (ValidityKey)i+1, obj->until() );
  } else {
    CPPUNIT_ASSERT_EQUAL_MESSAGE( ( s.str() + "until" ).c_str(), 
                                  ValidityKeyMax, obj->until() );
  }
}
\endcode



\section mv_rw Multi Version Storage and Retrieval

Multi version storage and retrieval is practically identical to the single version mode. The only difference is the \c FolderVersioning::MULTI_VERSION flag that one has to specify during folder creation:

\code
IFolderPtr folder = db->createFolder( "/myfolder", 
                                      payloadSpec1,
                                      "my description",
                                      FolderVersioning::MULTI_VERSION );

folder->setupStorageBuffer();
folder->storeObject( 0, 4, dummyPayload( 0 ) );
folder->storeObject( 2, 6, dummyPayload( 1 ) );
folder->flushStorageBuffer();
\endcode

The corresponding \c FolderVersioning::SINGLE_VERSION flag has been omitted in the single version example since it is a default argument of the \c createFolder method.

Note that there is no restriction with respect to overlapping IOVs in the multi version mode.

The retrieval (single object or bulk) is identical to the single version mode:

\code
IObjectIteratorPtr objs = folder->browseObjects( ValidityKeyMin,
                                                 ValidityKeyMax );
\endcode


\section tag_w Tagging (Tag Storage)

Assuming that objects have been stored in a folder:

\code
IFolderPtr folder = db->createFolder( "/myfolder", 
                                      payloadSpec1,
                                      "my description",
                                      FolderVersioning::MULTI_VERSION );
      
folder->setupStorageBuffer();
folder->storeObject( 0, 4, dummyPayload( 0 ) );
folder->storeObject( 2, 6, dummyPayload( 1 ) );
folder->flushStorageBuffer();
\endcode

one can tag the "HEAD" of the folder by calling its \c tag method:

\code
folder->tag( "tagA", "an optional description" );
\endcode

Note that tagging is only available in multi version mode.

It is also possible to tag the HEAD of a folder as it was at a certain point in time:

\code
sea::Time asOfDate = seal::Time::current();
folder->tag( asOfDate, "tagB" );
\endcode

Note that the server will interpret the given time as its local time. Be aware of clock skew or time zones when supplying the \c asOfDate argument.

Optionally one can obtain the time from the insertion time of an object:

\code
ValidityKey pointInTime = 0;
IObjectPtr obj = folder->findObject( pointInTime );
seal::Time asOfDate = obj->insertionTime();
\endcode

This makes sure the time is in the context of the server time frame.


\section tag_r Reading from Tags (Tag Retrieval)

Reading from a tag is similar to normal browsing. Just provide the tag string as an additional agrument:

\code
IObjectIteratorPtr objs = folder->browseObjects( ValidityKeyMin,
                                                 ValidityKeyMax,
                                                 (IChannelId)0,
                                                 "tagA" );
      
CPPUNIT_ASSERT_EQUAL_MESSAGE( "object count", 2ul, objs->size() );

IObjectPtr obj = objs->next();
CPPUNIT_ASSERT_MESSAGE( "obj 1 payload", 
                        dummyPayload( 0 ) == obj->payload() );
CPPUNIT_ASSERT_EQUAL_MESSAGE( "obj 1 since",
                              (ValidityKey)0, obj->since() );
CPPUNIT_ASSERT_EQUAL_MESSAGE( "obj 1 until",
                              (ValidityKey)2, obj->until() );
\endcode

*/

}
