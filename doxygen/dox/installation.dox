namespace cool {

/*! \page installation Installation Instructions



\section scram_afs SCRAM/AFS based installation

For detailed installation instructions with scram at CERN please refer to the following webpage:

  http://lcgapp.cern.ch/project/CondDB/snapshot/gettingStartedCOOL.html





\section scram_standalone Standalone SCRAM based installation

The LCG software can be installed on standalone systems (i.e. those without access to CERN's AFS) by downloading the required packages through the LCG installation manager. The following webpage describes the process:

http://spi.cern.ch/workbook/howto/HowTo-Install-locally-lcg-software.html

After installing POOL via the LCG installation manager (which also installs its dependency SEAL), proceed as follows:

- set the SITENAME environment variable to STANDALONE, e.g.
  \code setenv SITENAME STANDALONE \endcode

- follow the instructions under the section \ref scram_afs

- the project setup phase might fail if your local installation paths differ from those specified in the <tt> tools-STANDALONE.conf </tt> file

- the tools file can be edited 
  \code COOL_X_Y_Z/config/site/tools-STANDALONE.conf \endcode

- after changes to this file rerun \code scram setup \endcode to update the project paths





\section scram_osx SCRAM based installation on Mac OSX

COOL can be built with scram on OSX (standalone or AFS based). There are a few things to watch out for. Proceed as follows to install on OSX:

- \code setenv PATH /afs/cern.ch/sw/lcg/app/spi/scram:$PATH \endcode
  or
  \code setenv PATH /opt/sw/lcg/app/spi/scram:$PATH \endcode
  if you installed the LCG software locally as described in section \ref scram_standalone above. In this case also set SITENAME to STANDALONE (see above).
  
- \code setenv SCRAM_ARCH osx103_gcc33 \endcode

- \code unsetenv SCRAM_HOME \endcode

- \code unsetenv ORACLE_HOME \endcode

- \code cd ~/myLCG \endcode

- \code curl -O myBootStrapFileCoolSRC.COOL_X_Y_Z \
  "http://cool.cvs.cern.ch/cgi-bin/cool.cgi/cool/config/scram/BootStrapFileCoolSRC?rev=COOL_X_Y_Z" \endcode
  (use a release tag, e.g. \c COOL_1_0_2, or \c HEAD instead of \c COOL_X_Y_Z)

- Unless you have anonymously logged into the cool and spi cvs servers before, your .cvspass file does not contain the login credentials. Run the commands
  \code cvs -d :pserver:anonymous@cool.cvs.cern.ch:/cvs/COOL login \endcode
  \code cvs -d :pserver:anonymous@SPITOOLS.cvs.cern.ch:/cvs/SPITOOLS login \endcode
  and press return at the password prompt.
    
- OSX' cvs client for some reason looks for the \c .cvspass under \c /tmp, in a directory \c ${uid}CVSmodule. The string \c ${uid} represents your user id, which you can determine by running the command \c id -u. OSX user ids start at 501, so on single user machines this directory will typically be
  \code /tmp/501CVSmodule \endcode
  In order to checkout the sources via CVS, the login credentials have to be copied to this directory:
  \code cp ~/.cvspass /tmp/`id -u`CVSmodule/ \endcode
  This is necesssary only once, when the package is installed. Note that \c /tmp contents are deleted after a reboot.

- Now the project can be bootstrapped
  \code scram project file:myBootStrapFileCoolSRC.COOL_X_Y_Z \endcode
  and built
  \code cd COOL_X_Y_Z/src/RelationalCool/src; scram b \endcode





\section scram_env Environment Variables Used by SCRAM

Running COOL application requires a few environment variables to be set. The following list comprises the environment variables that the command 
\code eval `scram runtime -csh` \endcode
which sets up the runtime environment sets. This can be useful to build and run COOL applications with other build systems than SCRAM. Note that the shown values are example values referring to a standalone OSX build.

SCRAMRT_SEAL_PLUGINS=/opt/sw/lcg/app/releases/SEAL/SEAL_1_6_2/osx103_gcc33/lib/modules:...

SCRAMRT_NLS_LANG=american_america.WE8ISO8859P9

SCRAMRT_ODBCSYSINI=/opt/sw/lcg/external/unixodbc/2.2.6/osx103_gcc33/etc

SCRAMRT_NLS_DATE_FORMAT=DD-MON-FXYYYY

SCRAMRT_LD_LIBRARY_PATH=/Users/sas/Projects/Atlas/myLCG/COOL_HEAD/osx103_gcc33/lib:...

SCRAMRT_PATH=/Users/sas/Projects/Atlas/myLCG/COOL_HEAD/osx103_gcc33/bin:...

SCRAMRT_PYTHONPATH=/opt/sw/lcg/external/Boost/1.31.0/osx103_gcc33/lib/python2.3/site-packages:...

SCRAMRT_TNS_ADMIN=/opt/oracle/network/admin

SCRAMRT_SEAL_KEEP_MODULES=true

SCRAMRT_ODBCINST=/opt/sw/lcg/external/unixodbc/2.2.6/osx103_gcc33

SEAL_PLUGINS=/opt/sw/lcg/app/releases/SEAL/SEAL_1_6_2/osx103_gcc33/lib/modules:...

NLS_LANG=american_america.WE8ISO8859P9

ODBCSYSINI=/opt/sw/lcg/external/unixodbc/2.2.6/osx103_gcc33/etc

NLS_DATE_FORMAT=DD-MON-FXYYYY

LD_LIBRARY_PATH=/Users/sas/Projects/Atlas/myLCG/COOL_HEAD/osx103_gcc33/lib:...

PYTHONPATH=/opt/sw/lcg/external/Boost/1.31.0/osx103_gcc33/lib/python2.3/site-packages:...

TNS_ADMIN=/opt/oracle/network/admin

SEAL_KEEP_MODULES=true

ODBCINST=/opt/sw/lcg/external/unixodbc/2.2.6/osx103_gcc33

LOCALRT=/Users/sas/Projects/Atlas/myLCG/COOL_HEAD


*/

}
