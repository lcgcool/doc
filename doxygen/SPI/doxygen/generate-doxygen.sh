#! /bin/bash

PLATF=slc3_gcc323

#-------------------

PATH=/usr/local/gcc-alt-3.2.3/bin:/afs/cern.ch/sw/lcg/app/spi/tools/0.6.1/scripts:/afs/cern.ch/sw/lcg/app/spi/scram:/opt/SUNWspro/bin/:/afs/cern.ch/user/l/lcgspi/scripts/doxygen:/usr/sue/bin:/usr/bin:/bin:/usr/bin/X11:/usr/local/bin:/usr/local/bin/X11:/cern/pro/bin:.:/afs/cern.ch/group/jp/scripts:/afs/cern.ch/sw/lcg/external/mysql/4.0.18/${PLATF}/bin
export PATH

LD_LIBRARY_PATH=/usr/local/gcc-alt-3.2.3/lib:/afs/cern.ch/sw/lcg/external/mysql/4.0.18/${PLATF}/lib:/usr/local/lib:/usr/lib:/afs/cern.ch/sw/lcg/external/graphviz/1.9/${PLATF}/lib/graphviz/
export LD_LIBRARY_PATH

#-------------------
rm -rf ${WORKTOP}/$PROJDIRTAG
mkdir -p ${WORKTOP}/$PROJDIRTAG/checkout
cd ${WORKTOP}/$PROJDIRTAG/checkout


cvs -Q -d :pserver:anonymous@${CVSPROJ}.cvs.cern.ch:/cvs/${CVSPROJ} co -r $PROJTAG ${CVSTOP} 


rm -rf ${WORKTOP}/$PROJDIRTAG/$PROJLOW
mv ${WORKTOP}/$PROJDIRTAG/checkout/${CVSTOP} ${WORKTOP}/$PROJDIRTAG/$PROJLOW
rm -rf ${WORKTOP}/$PROJDIRTAG/checkout

mkdir -p  ${WORKTOP}/$PROJDIRTAG/$PROJLOW/config/doxygen

if [[ -e ${DOXYFILECFG} ]]; then
    echo "---------- Use doxygen configuration file ${DOXYFILECFG}"
    cp -f ${DOXYFILECFG}  ${STARTDIR}/doxygen_${PROJ}_config/Doxyfile_${PROJ}.cfg
else 
  if [[ -e ${WORKTOP}/$PROJDIRTAG/$PROJLOW/config/doxygen/Doxyfile_${PROJ}.cfg ]]; then
    echo "---------- Use doxygen configuration file Doxyfile_${PROJ}.cfg from CVS"
   cp -f ${WORKTOP}/$PROJDIRTAG/$PROJLOW/config/doxygen/Doxyfile_${PROJ}.cfg  ${STARTDIR}/doxygen_${PROJ}_config
  else
    echo "---------- Use default doxygen configuration file Doxyfile_${PROJ}.cfg"
  fi
fi

cp -rf ${STARTDIR}/doxygen_${PROJ}_config/* ${WORKTOP}/$PROJDIRTAG/$PROJLOW/config/doxygen

cd ${WORKTOP}/$PROJDIRTAG/$PROJLOW/config/doxygen

mkdir -p ../../doc/doxygen

\mv Doxyfile_${PROJ}.cfg Doxyfile_${PROJ}.cfg.old
cat Doxyfile_${PROJ}.cfg.old | sed "s|/build/lcgspi/doxygen/|${WORKTOP}|" > Doxyfile_${PROJ}.cfg

/afs/cern.ch/sw/lcg/external/doxygen/1.4.1/${PLATF}/bin/doxygen ./Doxyfile_${PROJ}.cfg

rm -rf ${OUTDIRTOP}/$PROJDIRTAG/doxygen


mkdir -p ${OUTDIRTOP}/$PROJDIRTAG/doxygen

if [[ -e ${WORKTOP}/$PROJDIRTAG/$PROJLOW/doc/doxygen/html/reference_tags.xml ]]; then
  python ${STARTDIR}/../repl_string.py ${WORKTOP}/$PROJDIRTAG/$PROJLOW/doc/doxygen/html/reference_tags.xml ${WORKTOP}/$PROJDIRTAG/$PROJLOW/ /afs/cern.ch/sw/lcg/app/releases/$PROJ/$PROJDIRTAG/src/ 
fi

echo cp -rf ${WORKTOP}/$PROJDIRTAG/$PROJLOW/doc/doxygen/html ${OUTDIRTOP}/$PROJDIRTAG/doxygen/
cp -rf ${WORKTOP}/$PROJDIRTAG/$PROJLOW/doc/doxygen/html ${OUTDIRTOP}/$PROJDIRTAG/doxygen/


if [[ -e ${OUTDIRTOP}/$PROJDIRTAG/doxygen/html/reference_tags.xml ]]; then
  echo mv ${OUTDIRTOP}/$PROJDIRTAG/doxygen/html/reference_tags.xml ${OUTDIRTOP}/$PROJDIRTAG/doxygen/html/${PROJDIRTAG}_reference_tags.xml
  mv ${OUTDIRTOP}/$PROJDIRTAG/doxygen/html/reference_tags.xml ${OUTDIRTOP}/$PROJDIRTAG/doxygen/html/${PROJDIRTAG}_reference_tags.xml

  ln -s ${OUTDIRTOP}/$PROJDIRTAG/doxygen/html/${PROJDIRTAG}_reference_tags.xml  ${OUTDIRTOP}/$PROJDIRTAG/doxygen/html/${PROJ}_reference_tags.xml

fi

lynx -dump -nolist ${OUTWWWTOP}/$PROJDIRTAG/doxygen/html/

if [[ "$KEEPWORKTOP" != "yes" ]]; then 
  echo rm -rf ${WORKTOP}/$PROJDIRTAG
  rm -rf ${WORKTOP}/$PROJDIRTAG
fi

#echo emacs ${OUTDIRTOP}/index.html
echo "Documentation is available on the Web at ${OUTWWWTOP}/$PROJDIRTAG/doxygen/html/"
