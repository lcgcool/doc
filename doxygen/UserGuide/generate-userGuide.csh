#! /bin/csh -f

foreach theFile ( `ls *.tex` )
  cat $theFile \
    | sed 's|\\section|\\chapter|g' \
    | sed 's|\\subsection|\\section|g' \
    | sed 's|\\subsubsection|\\subsection|g' \
    | sed 's|\\paragraph|\\subsubsection|g' \
    | sed 's|\\includegraphics{|\\includegraphics[width=14cm]{|g' \
    > $theFile.new
  \mv $theFile $theFile.old
  \mv $theFile.new $theFile
end

cat refman.tex \
  | awk 'BEGIN{ok=0; buf=""}{if ($1=="\\printindex") print "\\appendix" ORS buf}{if ($2=="File" && $3=="Documentation}") ok=1}{if (ok==1) print $0; else if ($2=="Class" && $3=="Documentation}") buf=$0; else if (buf!="") buf=buf ORS $0; else print $0}' \
  > refman.tex.new
\mv refman.tex.new refman.tex

cat refman.tex \
  | sed 's|{article}|{book}|g' \
  | sed 's|tableofcontents|tableofcontents\\addcontentsline{toc}{chapter}{Contents}|g' \
  | sed 's|COOL Package Documentation|Introduction|g' \
  | grep -v 'COOL Namespace Documentation' \
  | grep -v 'input{namespacecool' \
  | grep -v 'COOL Page Documentation' \
  | grep -v 'COOL Directory Documentation' \
  | grep -v 'input{dir_' \
  | grep -v 'COOL File Documentation' \
  | egrep -v 'input.*_8' \
  | sed "s/classcool/CLASSCOOL/g" \
  | grep -v 'input{class' \
  | sed "s/CLASSCOOL/classcool/g" \
  | sed 's|Interface Description|C++ API Description|g' \
  | sed 's|COOL Class Documentation|C++ API Reference|g' \
  > refman.tex.new
\mv refman.tex.new refman.tex

foreach theFile ( `ls -1 *.tex | grep -v refman.tex | grep -v index.tex | grep -v class | grep -v dir_ | grep -v _8` )
  cat $theFile \
    | sed 's|\\section|\\chapter|g' \
    | sed 's|\\subsection|\\section|g' \
    | sed 's|\\subsubsection|\\subsection|g' \
    | sed 's|\\paragraph|\\subsubsection|g' \
    > $theFile.new
  \mv $theFile.new $theFile
end

gmake

# Create a pdf document with 2 pages per sheet
# http://www.tug.org/pipermail/tugindia/2004-November/003139.html
# http://www.tug.org/tex-archive/macros/latex/contrib/pdfpages/pdfpages.pdf
cat > refman2.tex <<EOF
\documentclass[a4paper]{article}
\usepackage{pdfpages}
\begin{document}
\includepdf
[pages={,1,,2-LAST},nup=1x2,frame=true,landscape=true,openright=false]{refman}
\end{document}
EOF

# Count PDF document pages
# http://two.pairlist.net/pipermail/reportlab-users/2004-March/002797.html
set theLast=`pdf2ps -sOutputFile=- refman.pdf | grep -c "%%Page: "`
cat refman2.tex \
  | sed "s|LAST|$theLast|" \
  > refman2.tex.new
\mv refman2.tex.new refman2.tex

pdflatex refman2
