#! /bin/sh

#echo "You must comment out this line!"; exit 1

PROJTAG=HEAD

# Use this for producing the public HEAD UserGuide
#PROJDIRTAG=snapshot
# Use this for producing the private HEAD UserGuide
PROJDIRTAG=tmp

PROJ=COOL
PROJLOW=cool

CVSPROJ=COOL
CVSTOP=cool

# Directory where the doxygen documentation will be built
###OUTDIRTOP=/afs/cern.ch/sw/lcg/app/spi/doc/doxygen/output/${PROJ}
OUTDIRTOP=/afs/cern.ch/project/lcg/app/www/CondDB/CoolUserGuide

# WWW address where the doxygen documentation will be visible
###OUTWWWTOP=http://lcgapp.cern.ch/doxygen/${PROJ}
OUTWWWTOP=http://lcgapp.cern.ch/project/CondDB/CoolUserGuide

# Temporary working area
###WORKTOP=/build/lcgspi/doxygen
WORKTOP=/opt/${USER}/tmp/userGuide

# Directory $WORKTOP/$PROJDIRTAG is removed at the end unless KEEPWORKTOP=yes
###KEEPWORKTOP=
KEEPWORKTOP=yes

# Location of the scripts to generate the doxygen documentation
###STARTDIR=~lcgspi/scripts/doxygen
STARTDIR=~/myLCG/COOL_HEAD/doc/doxygen/SPI/doxygen

# Documentation is generated using config file ${DOXYFILECFG} if it exists;
# else using config/doxygen/Doxyfile_${PROJ}.cfg from project CVS if it exists;
# else using ${STARTDIR}/doxygen_${PROJ}_config (which always exists)
###DOXYFILECFG=
DOXYFILECFG=~/myLCG/COOL_HEAD/doc/doxygen/UserGuide/Doxyfile_COOL_UserGuide 

# Export environment variables used inside the ${DOXYFILECFG} config file
export PROJDIRTAG
export WORKTOP

date
echo "=============== GENERATE DOXYGEN BEGIN"
source $STARTDIR/generate-doxygen.sh
echo "=============== GENERATE DOXYGEN END"
date

# Remove configuration file copied from DOXYFILECFG or CVS
# (the SPI team should comment this out!)
\rm ${STARTDIR}/doxygen_${PROJ}_config/Doxyfile_${PROJ}.cfg

echo "=============== GENERATE USERGUIDE BEGIN"
cd ${WORKTOP}/$PROJDIRTAG/$PROJLOW/doc/doxygen/latex
$STARTDIR/../../UserGuide/generate-userGuide.csh
\cp refman.pdf ${OUTDIRTOP}/$PROJDIRTAG
\cp refman2.pdf ${OUTDIRTOP}/$PROJDIRTAG
echo "=============== GENERATE USERGUIDE END"
date

echo "User Guide is available at "
echo "->  ${OUTWWWTOP}/$PROJDIRTAG/refman.pdf"
echo "->  ${OUTWWWTOP}/$PROJDIRTAG/refman2.pdf"

