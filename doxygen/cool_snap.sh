#! /bin/sh

echo "You must comment out this line!"; exit 1

PROJTAG=HEAD
PROJDIRTAG=COOL_snapshot

PROJ=COOL
PROJLOW=cool

CVSPROJ=COOL
CVSTOP=cool

# Directory where the doxygen documentation will be built
#OUTDIRTOP=/afs/cern.ch/sw/lcg/app/spi/doc/doxygen/output/${PROJ}
OUTDIRTOP=/afs/cern.ch/project/lcg/app/www/CondDB/tmp/doxygen

# WWW address where the doxygen documentation will be visible
#OUTWWWTOP=http://lcgapp.cern.ch/doxygen/${PROJ}
OUTWWWTOP=http://lcgapp.cern.ch/project/CondDB/tmp/doxygen

# Temporary working area
#WORKTOP=/build/lcgspi/doxygen
WORKTOP=/opt/${USER}/tmp/doxygen

# Directory $WORKTOP/$PROJDIRTAG is removed at the end unless KEEPWORKTOP=yes
#KEEPWORKTOP=
KEEPWORKTOP=yes

# Location of the scripts to generate the doxygen documentation
#STARTDIR=~lcgspi/scripts/doxygen
STARTDIR=~/myLCG/COOL_HEAD/doc/doxygen/SPI/doxygen

# Documentation is generated using config file ${DOXYFILECFG} if it exists;
# else using config/doxygen/Doxyfile_${PROJ}.cfg from project CVS if it exists;
# else using ${STARTDIR}/doxygen_${PROJ}_config (which always exists)
#DOXYFILECFG=
DOXYFILECFG=~/myLCG/COOL_HEAD/config/doxygen/Doxyfile_COOL.cfg

echo "=============== GENERATE DOXYGEN BEGIN"
source $STARTDIR/generate-doxygen.sh
echo "=============== GENERATE DOXYGEN END"

# Remove configuration file copied from DOXYFILECFG or CVS
# (the SPI team should comment this out!)
\rm ${STARTDIR}/doxygen_${PROJ}_config/Doxyfile_${PROJ}.cfg
