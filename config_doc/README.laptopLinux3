-----------------------------------------------------------------------
Standalone installation on an SLC4 Linux node with no AFS (26.03.2007)
-----------------------------------------------------------------------

=======================================================================
LAM
=======================================================================

1. As avalassi@lxb0674:

cd /tmp
tar -cvf /tmp/lam711.tar \
  /afs/cern.ch/project/dbgroup/scratch/avalassi/lam/lam-7.1.1-install
gzip lam711.tar
scp -2 lam711.tar.gz oracle@itrac403:/data/Cool

2. As oracle@itrac403:

cd /data/Cool
gunzip lam711.tar.gz
tar -xvf lam711.tar
mv afs/cern.ch/project/dbgroup/scratch/avalassi/lam .
rm -rf afs
rm lam711.tar

Create /data/Cool/lam/setupLAM.csh with the following lines:

setenv LAMINSTALL /data/Cool/lam/lam-7.1.1-install
if ( ! ${?LD_LIBRARY_PATH} ) setenv LD_LIBRARY_PATH
if ( ! ${?MANPATH} ) setenv MANPATH
setenv PATH ${LAMINSTALL}/bin:${PATH}
setenv LD_LIBRARY_PATH ${LAMINSTALL}/lib:${LD_LIBRARY_PATH}
setenv MANPATH ${MANPATH}:${LAMINSTALL}/man
setenv LAMHOME ${LAMINSTALL}

Create /data/Cool/lam/setupLAM.sh with the following lines:

export LAMINSTALL=/data/Cool/lam/lam-7.1.1-install
export PATH=${LAMINSTALL}/bin:${PATH}
export LD_LIBRARY_PATH=${LAMINSTALL}/lib:${LD_LIBRARY_PATH}
export MANPATH=${MANPATH}:${LAMINSTALL}/man
export LAMHOME=${LAMINSTALL}

Create ~/.cshrc with the following lines:

alias lo     'exit'
alias rm     'rm -i'
alias mv     'mv -i'
alias cp     'cp -i'
bindkey "\e[3~" backward-delete-char
set prompt='%{\033]0;%m:%~\007%}%{\033[7m%}[%n@%m tcsh]%{\033[0m%} %~ %# '
source /data/Cool/lam/setupLAM.csh
set autolist

Add the following lines to ~/.bashrc ON ALL 12 NODES!

echo "source /data/Cool/lam/setupLAM.sh" >> ~/.bashrc

Create /data/Cool/lam/lamCool.bhost with the following lines:

itrac403 cpu=10
itrac404 cpu=10
itrac405 cpu=10
itrac406 cpu=10
itrac407 cpu=10
itrac408 cpu=10
itrac415 cpu=10
itrac416 cpu=10
itrac417 cpu=10
itrac418 cpu=10
itrac419 cpu=10
itrac420 cpu=10

Now you can boot the LAM network:

lamboot -v lamCool.bhost

=======================================================================
COOL
=======================================================================

Inspired from the Windows standalone installation 
documented in RelationalCool/doc/README.standalone.

Used my own script. This creates a script on the laptop
(the idea for Windows was that SCRAM_ARCH was different),
executed on a central node to create a tar file of AFS.
The tar is then shipped back to the laptop.
This is to avoid file copying over AFS on the laptop.

I also use my own script to download CORAL locally.
I use it as above, to create tarballs that I then copy.

1. As avalassi@lxb0674:

Create the externals script:

mkdir -p /opt/sw
cd ~avalassi/myLCG/COOL_HEAD/src/config/scram
./coolMirrorExternals.csh

Execute the externals script (this takes ~11 minutes and creates a 1.6 GB tar):

./coolMirrorExternals-slc4_ia32_gcc34.csh 

Copy the externals tar file (this takes ~3 minutes):

date
scp -2 \
  /opt/avalassi/coolKit-slc4_ia32_gcc34/sw/lcg-slc4_ia32_gcc34.tar\
  oracle@itrac403:/data/Cool
date

Create a COOL tar files from the current HEAD:

cd ~avalassi/myLCG/COOL_HEAD/
tar -cvf src.tar src
tar -cvf SCRAM.tar .SCRAM

Copy the COOL tar files (this takes ~2 minutes):

date
scp -2 ~avalassi/myLCG/COOL_HEAD/src.tar oracle@itrac403:/data/Cool
scp -2 ~avalassi/myLCG/COOL_HEAD/SCRAM.tar oracle@itrac403:/data/Cool
date

2. As oracle@itrac403:

Untar the externals tar:

mkdir /data/Cool/sw
cd /data/Cool/sw
rm -rf /data/Cool/sw/lcg
tar -xvf ../lcg-slc4_ia32_gcc34.tar > ../lcg-slc4_ia32_gcc34.txt

Add the following to .cshrc:

setenv PATH /data/Cool/sw/lcg/app/spi/scram:$PATH
setenv SCRAM_ARCH slc4_ia32_gcc34
setenv SITENAME RAC4

Edit /data/Cool/sw/lcg/app/spi/scram/scram and add the following line:

$ENV{SCRAM_HOME}="/data/Cool/sw/lcg/app/spi/scram/V0_20_0";

Untar the COOL tar:

mkdir /data/Cool/COOL_HEAD
cd /data/Cool/COOL_HEAD
rm -rf /data/Cool/COOL_HEAD/.SCRAM
rm -rf /data/Cool/COOL_HEAD/src
tar -xvf ../SCRAM.tar
tar -xvf ../src.tar

Setup and build COOL:

cd /data/Cool/COOL_HEAD/src
scram setup
scram b

Create the authentication.xml file:

cd /data/Cool/COOL_HEAD/src/PerformanceTests/PDBStressTest
vi authentication.xml

=======================================================================
PDB stress tests
=======================================================================

As oracle@itrac403.

Run the stress test once (this takes 10.6s, better than lxplus!)

cd /data/Cool/COOL_HEAD/src/PerformanceTests/PDBStressTest
eval `scram runtime -sh`
python pdbStressTest.py stressTest2 0

Boot LAM, using itrac403 as LAM master (it must be one of the LAM hosts):

lamboot -v lamCool.bhost 

For a simple LAM test:

lamexec C /data/Cool/COOL_HEAD/src/PerformanceTests/PDBStressTest/lamWrapper.csh hostname

To dispatch the stress test from itrac403 (master):

cd /data/Cool/COOL_HEAD/src/PerformanceTests/PDBStressTest
\rm test.out
lamexec C /data/Cool/COOL_HEAD/src/PerformanceTests/PDBStressTest/lamWrapper.csh /data/Cool/COOL_HEAD/src/PerformanceTests/PDBStressTest/pdbStressTest.csh >& test.out &

To kill all:

lamexec N ps -ef | grep oracle
lamexec N killall python
