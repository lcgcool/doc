------------------------------
Linux PC preparation for WINE
------------------------------

* AV 26/10/2005

To run WINE you need to have unlimited virtual memory.
Otherwise you may expect segmentation faults such as
  "wine: failed to initialize: 
  /opt/avalassi/wine/wine-[...]-install/lib/wine/ntdll.dll.so: 
  failed to map segment from shared object: Cannot allocate memory"

In order to be able to change the virtual memory runtime 'soft' limit,
you need to change the 'hard' limit in a system file as root: you must add
  "avalassi              -       as      unlimited"
in 
  "/etc/security/limits.conf"
I do this on lxb0675 (present limit is 768000 kBytes).
(NB It is necessary to ssh again for PAM to use the new "hard" limit).
The soft limit can be changed using "ulimit" up to the hard limit set by root.

---------------------------------------------------
Prepare archives of files copied from a Windows PC
---------------------------------------------------

* AV 26/10/2005

Open a cygwin window and prepare the archives:
  cd "/cygdrive/c/Program Files/Microsoft Visual Studio .NET 2003"
  tar -cvf ~/wineMSVS.tar \
     Common7/IDE/ Common7/Packages/ Common7/Tools/ \
     Vc7/bin/ Vc7/include/ Vc7/lib/ Vc7/PlatformSDK 
  cd /cygdrive/c/WINNT/system32
  tar -cvf ~/wineMSVC.tar \
    msvcp71.dll msvcp71d.dll msvcp71d.pdb \
    msvcr71.dll msvcr71.pdb msvcr71d.dll msvcr71d.pdb 
  cd /cygdrive/c/WINNT
  tar -cvf ~/wineFonts.tar Fonts
  gzip ~/wineMSVS.tar
  gzip ~/wineMSVC.tar
  gzip ~/wineFonts.tar

From lxb0675 copy the archives:
  mkdir -p /opt/avalassi/wineDownloads
  cd /opt/avalassi/wineDownloads
  scp avalassi@pcitdb38:/cygdrive/l/home/avalassi/wineMSVS.tar.gz .
  scp avalassi@pcitdb38:/cygdrive/l/home/avalassi/wineMSVC.tar.gz .
  scp avalassi@pcitdb38:/cygdrive/l/home/avalassi/wineFonts.tar.gz .

---------------------------------------------------
Prepare a registry file with the relevant changes
---------------------------------------------------

* AV 18/10/2006

Create the file 

  /opt/avalassi/wineDownloads/RegistryChanges.reg

with the following content

REGEDIT4

[HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment]
"INCLUDE"="c:\\program files\\microsoft visual studio .net 2003\\vc7\\include"
"LIBRARY"="c:\\program files\\microsoft visual studio .net 2003\\vc7\\lib"
"PATH"="c:\\program files\\microsoft visual studio .net 2003\\common7\\ide;c:\\program files\\microsoft visual studio .net 2003\\vc7\\bin;c:\\windows\\system32;c:\\windows"

[HKEY_LOCAL_MACHINE\Software\Microsoft\VisualStudio\7.1]
"InstallDir"="c:\\program files\\microsoft visual studio .net 2003\\common7\\ide"

-------------
AFS problems
-------------

* AV 17/01/2006

I suddenly cannot see my private AFS files anymore.
I do not understand what has changed.
I can only enter directories with "system:anyuser l" ACL
and see files with "system:anyuser rl" ACL.

I cannot even see my home directory anymore!
To test, just do
  cd
  echo pippo > pippo
  wcmd
  cat pippo
  
* AV 17/01/2006

Finally understood the problem: there was an old wineserver process
from a few days ago, half dead. This is probably the process that
all wine commands need to use to access the unix filesystem.
Since it was a few days old, the AFS tokens had expired on it
(and were not renewed by running klog, since it was disconnected
from the local shell process tree).

Killing the stale wineserver was enough.
Note that every new wine command starts at least one wineserver,
but this dies automatically when all wine commands terminate.
The stale wineserver was probably created when I killed the wrong
process to let gccxml complete successfully the PyCool build.

-----------------------
OpenGL and WINE 0.9.22
-----------------------

* AV 18/10/2006

In Wine 0.9.20 and 0.9.21 many OpenGL features were added.
As a consequence, I now get warnings at runtime such as the ones below.
I am 100% sure that this is new between 0.9.11 and 0.9.22 because
I tried the same exact COOL test with the old and new wine,
and the only differences are these warnings for the new Wine.

 Xlib:  extension "GLX" missing on display "lxb0675.cern.ch:10.0".
 err:wgl:X11DRV_WineGL_InitOpenglInfo  couldn't initialize OpenGL, expect 
 problems
 err:wgl:has_opengl Intialization of OpenGL info failed, disabling OpenGL! 

In his recipe, Marco recommends to use the --disable-opengl flag 
at the end of configure in order to disable openGL and fix this problem.
However I did not notice any significant difference with or without it.

Instead, I took the opportunity to upgrade my Exceed server.
The reason why these warnings appear is that my old Exceed9 server
does not support OpenGL: the Exceed 3D extension is needed.
I therefore upgraded to Exceed2006 (ie Exceed 11) with the 3D extension.
Support for OpenGL must be explicitly enabled in Xconfigure;
in particular, GLX 1.3 is needed (GLX 1.2 triggers different warnings).

In order to check whether the X server supports GLX 
the following command is needed:
  xdpyinfo

Note finally that with previous wine and COOL versions 
I saw 'import ROOT' fail under WINE as ROOT tries to load 
the OpenGL graphics libraries. This was cured at the time 
by using PyCool against ROOT 5.11.02 or later.

----------------------------
Install WINE 0.9.22 on SLC3
----------------------------

* AV 17/10/2006

Install Wine 0.9.22: Marco used this version (with extra patches)
in order to be able to run the COOL test suites on Wine.
See also Marco's instructions on
  http://lcgapp.cern.ch/project/CondDB/snapshot/wineHowTo.html

First make sure you have killed all instances of wine:

  killall wine-preloader

Check using:

  ps -efl | grep wine

Follow the same recipe that was used to install the previous wine on lxb0675.
Download and untar:

  mkdir /opt/avalassi/wine0922
  cd /opt/avalassi/wine0922
  wget "http://switch.dl.sourceforge.net/sourceforge/wine/wine-0.9.22.tar.bz2"
  bunzip2 wine-0.9.22.tar.bz2 
  tar xvf wine-0.9.22.tar

Remove the tar archive and copy again the bz2 version:

  cd /opt/avalassi/wine0922
  wget "http://switch.dl.sourceforge.net/sourceforge/wine/wine-0.9.22.tar.bz2"
  \rm wine-0.9.22.tar

Download and apply Marco's patch

  cd /opt/avalassi/wine0922
  wget -O marco.patch "http://cool.cvs.cern.ch/cgi-bin/cool.cgi/*checkout*/cool/doc/WebSite/wine-0.9.22-COOL.patch?rev=HEAD&content-type=text/plain"
  patch -p0 -i marco.patch
  cd /opt/avalassi/wine0922
  du -sm *

There are now 11 MB in the bz2 and 87 MB in the untarred directory.
Configure (this takes ~2 minutes):

  cd /opt/avalassi/wine0922/wine-0.9.22
  mkdir /opt/avalassi/wine0922/wine-0.9.22-install
  date
  ./configure --prefix=/opt/avalassi/wine0922/wine-0.9.22-install
  date
  cd /opt/avalassi/wine0922
  du -sm *

There are now 11 MB in the bz2, 92 MB untarred and 1 MB in install.
Make dependencies (this takes ~2 minutes):

  cd /opt/avalassi/wine0922/wine-0.9.22
  date
  make depend
  date
  cd /opt/avalassi/wine0922
  du -sm *

There are now 11 MB in the bz2, 94 MB untarred and 1 MB in install.
Build (this takes ~26 minutes): 

  cd /opt/avalassi/wine0922/wine-0.9.22
  date
  make -j 3
  date
  cd /opt/avalassi/wine0922
  du -sm *

There are now 11 MB in the bz2, 1036 MB untarred and 1 MB in install.
Install (this takes ~2 minutes):

  cd /opt/avalassi/wine0922/wine-0.9.22
  date
  make install
  date
  cd /opt/avalassi/wine0922
  du -sm *

There are now 11 MB in the bz2, 1036 MB untarred and 277 MB in install.
Setup the environment:

  setenv PATH \
    /opt/avalassi/wine0922/wine-0.9.22-install/bin:$PATH
  setenv LD_LIBRARY_PATH \
    /opt/avalassi/wine0922/wine-0.9.22-install/lib:$LD_LIBRARY_PATH
  limit vmemoryuse unlimited

Create a fake Windows installation.

  cd /opt/avalassi/wine0922
  \rm -rf /opt/avalassi/wine0922/win32
  mkdir /opt/avalassi/wine0922/win32
  setenv WINEPREFIX /opt/avalassi/wine0922/win32
  wineprefixcreate

This takes a while because it needs to load all fonts.
Create the appropriate directories.

  cd $WINEPREFIX/dosdevices
  rm -f z:
  ln -s /afs z:
  ln -s / e:
  \rm -rf /tmp/wine0922
  mkdir /tmp/wine0922
  ln -s /tmp/wine0922 t:

I copy the fonts and the VisualStudio tar files from the previous install:

  cd /opt/avalassi/wine0922
  cp /opt/avalassi/wineDownloads/wineMSVS.tar.gz .
  cp /opt/avalassi/wineDownloads/wineMSVC.tar.gz .
  cp /opt/avalassi/wineDownloads/wineFonts.tar.gz .
  mkdir -p \
   "$WINEPREFIX/dosdevices/c:/Program Files/Microsoft Visual Studio .NET 2003"
  cp wineMSVS.tar.gz \
   "$WINEPREFIX/dosdevices/c:/Program Files/Microsoft Visual Studio .NET 2003"
  cp wineMSVC.tar.gz \
   "$WINEPREFIX/dosdevices/c:/windows/system32"
  cp wineFonts.tar.gz \
   "$WINEPREFIX/dosdevices/c:/windows"
  cd \
   "$WINEPREFIX/dosdevices/c:/Program Files/Microsoft Visual Studio .NET 2003"
  tar -xvzf wineMSVS.tar.gz
  \rm wineMSVS.tar.gz
  cd "$WINEPREFIX/dosdevices/c:/windows/system32"
  tar -xvzf wineMSVC.tar.gz
  \rm wineMSVC.tar.gz
  cd "$WINEPREFIX/dosdevices/c:/windows"
  tar -xvzf wineFonts.tar.gz
  \rm wineFonts.tar.gz
  \rm -rf fonts
  mv Fonts fonts

Create a fake directory for fonts.
In previous installations, if I did not do this 
I could get an error message when starting regedit.

  mkdir /opt/avalassi/wine0922/wine-0.9.22-install/share/wine/fonts

Modify the registries. 

  cd /opt/avalassi/wine0922
  \cp /opt/avalassi/wineDownloads/RegistryChanges.reg .
  limit vmemoryuse unlimited
  regedit /e FullRegistryOld.reg
  regedit RegistryChanges.reg
  regedit /e FullRegistryNew.reg
  diff FullRegistryNew.reg FullRegistryOld.reg
  
You might also simply have modified the registry using the GUI via
  regedit
(1) In 
  HKLM\System\CurrentControlSet\Control\Session Manager\Environment
add to PATH (separated by ";")
  c:\program files\microsoft visual studio .net 2003\vc7\bin
  c:\program files\microsoft visual studio .net 2003\common7\ide
and create INCLUDE as
  c:\program files\microsoft visual studio .net 2003\vc7\include
and create LIBRARY as
  c:\program files\microsoft visual studio .net 2003\vc7\lib
(2) Create
  HKLM\Software\Microsoft\VisualStudio\7.1
and create InstallDir as
  c:\program files\microsoft visual studio .net 2003\common7\ide

----------------------------------
Build COOL on WINE 0.9.22 on SLC3
----------------------------------

* AV 17/10/2006

Add the following to the setupWine script:

  setenv PATH \
    /opt/avalassi/wine0922/wine-0.9.22-install/bin:$PATH
  setenv LD_LIBRARY_PATH \
    /opt/avalassi/wine0922/wine-0.9.22-install/lib:$LD_LIBRARY_PATH
  setenv WINEPREFIX /opt/avalassi/wine0922/win32
  limit vmemoryuse unlimited
  setenv SCRAM_ARCH win32_vc71_dbg_wine

Execute the setup script, then build COOL:

  cd ~/myLCG/COOL_HEAD/src
  scram setup
  scram b

------------------------
Run COOL on WINE 0.9.22
------------------------

* AV 17/10/2006

After Marco's latest changes (and a few very minor patches by Andrea)
it is now possible to run the three test suites for Windows under Wine.

Results:

1. RelationalCool

Everything works very smoothly!

2. PyCool

All the tests are successful!

However, sometimes the tests spit out a message such as
  "wine: Unhandled page fault on read access to 0x000000b4 
   at address 0x10aac8a (thread 0021), starting debugger..."
which may also be followed by a very long unfriednly stack trace.
This is under investigation.

3. PyCoolUtilities

Everything works very smoothly!

Note that the cmd_wrapper must be present also in the local 
execUnitTests.sh script (apart from thos in test_functions.sh, 
there are additional local calls to coolDropDB and coolAuthentication).

----------------------------------
DISABLE desktop double buffering!
----------------------------------

* AV 17/12/2006

Since some time I have noticed that the screen flickers when I start
a wine application - as if a screen capture was taking place.
I have also noticed that the most recent versions of Wine seem slower.

Today I launched all three test suites together, and the screen started
flickering like mad, essentially preventing me from working normally!

I found on the web that 'desktop double buffering' is related to this.
Actually many people suggest to turn it on to prevent the screen flickering
(with the reported tradeoff of increased memory consumption).
This used to be controlled by ~/.wine/config, but this is now managed by the 
winecfg tool, that stores the parameters directly in the Windows registry.

It turns out that desktop double buffering was enabled by default!
I instead tried to disable it, and everything is so much better! There 
is no screen flickering anymore... and everything runs 3 times faster!

For instance, "./wineWrap.sh unitTest_CoolKernel_Record.exe" runs in 
26 sec with desktop double buffering, and in only 8 seconds without it!

-----------------------------------------------------
FULL REINSTALLATION OF WINE 0.9.46 ON SLC4 (lxb0759)
-----------------------------------------------------

* AV 10/10/2007

As root, change the 'hard' limit to have unlimited virtual memory: add
  "avalassi              -       as      unlimited"
in "/etc/security/limits.conf".
I do this on lxb0759 (present limit is 768000 kBytes).
(NB It is necessary to ssh again for PAM to use the new "hard" limit).
The soft limit can be changed up to the hard limit set by root:  
  limit vmemoryuse unlimited
Unless the system file is changed, new limit is only 819200 kbytes:
it becomes unlimited if the system file is changed and after a new login.

* AV 10/10/2007

Copy the files already prepared on lxb0675:
  mkdir -p /opt/avalassi/wineDownloads
  cd /opt/avalassi/wineDownloads
  scp -2 avalassi@lxb0675:/opt/avalassi/wineDownloads/wineMSVS.tar.gz .
  scp -2 avalassi@lxb0675:/opt/avalassi/wineDownloads/wineMSVC.tar.gz .
  scp -2 avalassi@lxb0675:/opt/avalassi/wineDownloads/wineFonts.tar.gz .
  scp -2 avalassi@lxb0675:/opt/avalassi/wineDownloads/RegistryChanges.reg .

* AV 10/10/2007

Try to install the latest Wine 0.9.46 (instead of 0.9.22). First attempt.

The first attempt fails on wineprefixcreate (immediately) with:

  X Error of failed request:  BadRequest (invalid request code or no such operation)
  Major opcode of failed request:  139 (GLX)
  Minor opcode of failed request:  21 ()
  Serial number of failed request:  10
  Current serial number in output stream:  10

I imagine this may be a problem with OpenGL. So I decide this time to 
follow Marco's advice and disable OpenGL (even if I do have GLX in Exceed).

Note that wineprefixcreate failed (status=1), but it did create the directory
/opt/avalassi/wine0946/win32... so I could try to ignore the error and proceed.

* AV 10/10/2007

Try to install the latest Wine 0.9.46 (instead of 0.9.22). Second attempt.
This time I used ./configure --disable-opengl, but nothing changed.

Note also that I get the same error if I just run glxinfo, as suggested in
http://ubuntuforums.org/archive/index.php/t-236285.html
So it seems that wineprefixcreate is stil trying to use opengl...

Eventually I browsed a bit more: many posts quote --without-opengl
instead of --disable-opengl... indeed, the configure file greps
this string, not the other! So I will try a third time...

* AV 10/10/2007

First make sure you have killed all instances of wine:

  killall wine-preloader

Check using:

  ps -efl | grep wine

Follow the same recipe that was used to install the previous wine on lxb0675.
Download and untar:

  mkdir -p /opt/avalassi/wine0946
  cd /opt/avalassi/wine0946
  wget "http://switch.dl.sourceforge.net/sourceforge/wine/wine-0.9.46.tar.bz2"
  bunzip2 wine-0.9.46.tar.bz2 
  tar xvf wine-0.9.46.tar

Remove the tar archive and copy again the bz2 version:

  cd /opt/avalassi/wine0946
  wget "http://switch.dl.sourceforge.net/sourceforge/wine/wine-0.9.46.tar.bz2"
  \rm wine-0.9.46.tar

Download and apply Marco's patch (in the modified version I created)

  cd /opt/avalassi/wine0946/wine-0.9.46
  cp dlls/netapi32/wksta.c dlls/netapi32/wksta.c.OLD
  cp dlls/ntdll/critsection.c dlls/ntdll/critsection.c.OLD
  wget -O marco.patch "http://cool.cvs.cern.ch/cgi-bin/cool.cgi/*checkout*/cool/doc/WebSite/wine-0.9.46-COOL.patch?rev=HEAD&content-type=text/plain"
  patch -p0 -i marco.patch
  mv marco.patch ..
  cd /opt/avalassi/wine0946
  du -sm *

There are now 12 MB in the bz2 and 97 MB in the untarred directory.
Configure, this time without OpenGL (this takes ~2 minutes):

  cd /opt/avalassi/wine0946/wine-0.9.46
  mkdir /opt/avalassi/wine0946/wine-0.9.46-install
  date
  ./configure --prefix=/opt/avalassi/wine0946/wine-0.9.46-install --without-opengl
  date
  cd /opt/avalassi/wine0946
  du -sm *

There are now 12 MB in the bz2, 103 MB untarred and 1 MB in install.
Make dependencies (this takes ~1 minute):

  cd /opt/avalassi/wine0946/wine-0.9.46
  date
  make depend
  date
  cd /opt/avalassi/wine0946
  du -sm *

There are now 12 MB in the bz2, 105 MB untarred and 1 MB in install.
Build (this takes ~31 minutes): 

  cd /opt/avalassi/wine0946/wine-0.9.46
  date
  make -j 3
  date
  cd /opt/avalassi/wine0946
  du -sm *

There are now 12 MB in the bz2, 390 MB untarred and 1 MB in install.
(Note that 0.9.22 libs on SLC3 were 1036 MB!... better compiler?)
(Note that with openGL it was 393 MB... so it did work this time!)
Install (this takes ~2 minutes):

  cd /opt/avalassi/wine0946/wine-0.9.46
  date
  make install
  date
  cd /opt/avalassi/wine0946
  du -sm *

There are now 12 MB in the bz2, 390 MB untarred and 107 MB in install.
Setup the environment:

  setenv PATH \
    /opt/avalassi/wine0946/wine-0.9.46-install/bin:$PATH
  setenv LD_LIBRARY_PATH \
    /opt/avalassi/wine0946/wine-0.9.46-install/lib:$LD_LIBRARY_PATH
  limit vmemoryuse unlimited
  limit

Create a fake Windows installation.
This is very fast and this time it succeeds!
It does not print any error with Fonts... maybe I do not need to load them?

  cd /opt/avalassi/wine0946
  \rm -rf /opt/avalassi/wine0946/win32
  mkdir /opt/avalassi/wine0946/win32
  setenv WINEPREFIX /opt/avalassi/wine0946/win32
  wineprefixcreate

Create the appropriate directories.

  cd $WINEPREFIX/dosdevices
  rm -f z:
  ln -s /afs z:
  ln -s / e:
  \rm -rf /tmp/wine0946
  mkdir /tmp/wine0946
  ln -s /tmp/wine0946 t:

I copy the VisualStudio tar files from the previous install:

  cd /opt/avalassi/wine0946
  cp /opt/avalassi/wineDownloads/wineMSVS.tar.gz .
  cp /opt/avalassi/wineDownloads/wineMSVC.tar.gz .
  mkdir -p \
   "$WINEPREFIX/dosdevices/c:/Program Files/Microsoft Visual Studio .NET 2003"
  cp wineMSVS.tar.gz \
   "$WINEPREFIX/dosdevices/c:/Program Files/Microsoft Visual Studio .NET 2003"
  cp wineMSVC.tar.gz \
   "$WINEPREFIX/dosdevices/c:/windows/system32"
  cd \
   "$WINEPREFIX/dosdevices/c:/Program Files/Microsoft Visual Studio .NET 2003"
  tar -xvzf wineMSVS.tar.gz
  \rm wineMSVS.tar.gz
  cd "$WINEPREFIX/dosdevices/c:/windows/system32"
  tar -xvzf wineMSVC.tar.gz
  \rm wineMSVC.tar.gz

Copy the Fonts too? Yes, because the fonts directory is empty at the moment.

  cd /opt/avalassi/wine0946
  cp /opt/avalassi/wineDownloads/wineFonts.tar.gz .
  cp wineFonts.tar.gz \
   "$WINEPREFIX/dosdevices/c:/windows"
  cd "$WINEPREFIX/dosdevices/c:/windows"
  tar -xvzf wineFonts.tar.gz
  \rm wineFonts.tar.gz
  \rm -rf fonts
  mv Fonts fonts

Create a fake directory for fonts.
In previous installations, if I did not do this 
I could get an error message when starting regedit.

  mkdir /opt/avalassi/wine0946/wine-0.9.46-install/share/wine/fonts

Modify the registries. 

  cd /opt/avalassi/wine0946
  \cp /opt/avalassi/wineDownloads/RegistryChanges.reg .
  limit vmemoryuse unlimited
  regedit /e FullRegistryOld.reg
  regedit RegistryChanges.reg
  regedit /e FullRegistryNew.reg
  diff FullRegistryNew.reg FullRegistryOld.reg

----------------------------------
Build COOL on WINE 0.9.46 on SLC4
----------------------------------

* AV 10/10/2007

Add the following to the setupWine script:

  setenv PATH \
    /opt/avalassi/wine0946/wine-0.9.46-install/bin:$PATH
  setenv LD_LIBRARY_PATH \
    /opt/avalassi/wine0946/wine-0.9.46-install/lib:$LD_LIBRARY_PATH
  setenv WINEPREFIX /opt/avalassi/wine0946/win32
  limit vmemoryuse unlimited
  setenv SCRAM_ARCH win32_vc71_dbg_wine

Execute the setup script, then build COOL:

  cd ~/myLCG/COOL_HEAD/src
  scram setup
  scram b

All is ok. I only needed to change for 0.9.46 that winepath -l is more strict,
so I had to use winepath -w instead in my unixToWinPath.py. At the same time 
for COOL221 I had to fix SCRAM after removing SEAL package granularity.

--------------------------------
Run COOL on WINE 0.9.46 on SLC4
--------------------------------

Tests look ok without database, but as soon as Oracle is touched,
they start spitting out messages as follows. Note also that again
part of the unit test output is omitted. I decided that it was
probably better to go back to wine0922 on SLC4 too for comparison.

=== Running 'test_RelationalCool_RalSequence' (oracle) ===
=== dbId: oracle://int5r_cool;schema=lcg_cool;dbname=COOLWE__ ===

fixme:advapi:LsaOpenPolicy ((null),0x349c0c,0x00000001,0x349ae4) stub
fixme:advapi:LsaClose (0xcafe) stub
Wed Oct 10 23:12:32 CEST 2007
fixme:advapi:LsaOpenPolicy ((null),0x349e60,0x00000001,0x349d38) stub
fixme:advapi:LsaClose (0xcafe) stub
fixme:advapi:LsaOpenPolicy ((null),0x349e40,0x00000001,0x349d18) stub
fixme:advapi:LsaClose (0xcafe) stub
fixme:advapi:LsaOpenPolicy ((null),0x349e40,0x00000001,0x349d18) stub
fixme:advapi:LsaClose (0xcafe) stub
cool::RalSequenceTest::test_createSequence
Wed Oct 10 23:12:40 CEST 2007
fixme:advapi:LsaOpenPolicy ((null),0x349c0c,0x00000001,0x349ae4) stub
fixme:advapi:LsaClose (0xcafe) stub

-----------------------------------------------------
FULL REINSTALLATION OF WINE 0.9.22 ON SLC4 (lxb0761)
-----------------------------------------------------

* AV 10/10/2007

As root, change the 'hard' limit to have unlimited virtual memory: add
  "avalassi              -       as      unlimited"
in "/etc/security/limits.conf".
I do this on lxb0761 (present limit is 768000 kBytes).
(NB It is necessary to ssh again for PAM to use the new "hard" limit).
The soft limit can be changed up to the hard limit set by root:  
  limit vmemoryuse unlimited
Unless the system file is changed, new limit is only 819200 kbytes:
it becomes unlimited if the system file is changed and after a new login.

* AV 10/10/2007

Copy the files already prepared on lxb0675:
  mkdir -p /opt/avalassi/wineDownloads
  cd /opt/avalassi/wineDownloads
  scp -2 avalassi@lxb0675:/opt/avalassi/wineDownloads/wineMSVS.tar.gz .
  scp -2 avalassi@lxb0675:/opt/avalassi/wineDownloads/wineMSVC.tar.gz .
  scp -2 avalassi@lxb0675:/opt/avalassi/wineDownloads/wineFonts.tar.gz .
  scp -2 avalassi@lxb0675:/opt/avalassi/wineDownloads/RegistryChanges.reg .

* AV 10/10/2007

Try to install Wine 0.9.22 again, with no OpenGL. First attempt.

First make sure you have killed all instances of wine:

  killall wine-preloader

Check using:

  ps -efl | grep wine

Follow the same recipe that was used to install the previous wine on lxb0675.
Download and untar:

  mkdir -p /opt/avalassi/wine0922
  cd /opt/avalassi/wine0922
  wget "http://switch.dl.sourceforge.net/sourceforge/wine/wine-0.9.22.tar.bz2"
  bunzip2 wine-0.9.22.tar.bz2 
  tar xvf wine-0.9.22.tar

Remove the tar archive and copy again the bz2 version:

  cd /opt/avalassi/wine0922
  wget "http://switch.dl.sourceforge.net/sourceforge/wine/wine-0.9.22.tar.bz2"
  \rm wine-0.9.22.tar

Download and apply Marco's patch

  cd /opt/avalassi/wine0922/wine-0.9.22
  \cp dlls/netapi32/wksta.c dlls/netapi32/wksta.c.OLD
  \cp dlls/ntdll/critsection.c dlls/ntdll/critsection.c.OLD
  cd /opt/avalassi/wine0922
  wget -O marco.patch "http://cool.cvs.cern.ch/cgi-bin/cool.cgi/*checkout*/cool/doc/WebSite/wine-0.9.22-COOL.patch?rev=HEAD&content-type=text/plain"
  patch -p0 -i marco.patch
  mv marco.patch ..
  cd /opt/avalassi/wine0922
  du -sm *

There are now 12 MB in the bz2 and 87 MB in the untarred directory.
Configure, this time without OpenGL (this takes ~2 minutes):

  cd /opt/avalassi/wine0922/wine-0.9.22
  mkdir /opt/avalassi/wine0922/wine-0.9.22-install
  date
  ./configure --prefix=/opt/avalassi/wine0922/wine-0.9.22-install --without-opengl
  date
  cd /opt/avalassi/wine0922
  du -sm *

There are now 11 MB in the bz2, 92 MB untarred and 1 MB in install.
Make dependencies (this takes ~1 minute):

  cd /opt/avalassi/wine0922/wine-0.9.22
  date
  make depend
  date
  cd /opt/avalassi/wine0922
  du -sm *

There are now 11 MB in the bz2, 94 MB untarred and 1 MB in install.
Build (this takes ~30 minutes): 

  cd /opt/avalassi/wine0922/wine-0.9.22
  date
  make -j 3
  date
  cd /opt/avalassi/wine0922
  du -sm *

There are now 11 MB in the bz2, 990 MB untarred and 1 MB in install.
(Note that 0.9.22 libs on SLC3 were 1036 MB!... better compiler?)
Install (this takes ~2 minutes):

  cd /opt/avalassi/wine0922/wine-0.9.22
  date
  make install
  date
  cd /opt/avalassi/wine0922
  du -sm *

There are now 11 MB in the bz2, 990 MB untarred and 261 MB in install.
Setup the environment:

  setenv PATH \
    /opt/avalassi/wine0922/wine-0.9.22-install/bin:$PATH
  setenv LD_LIBRARY_PATH \
    /opt/avalassi/wine0922/wine-0.9.22-install/lib:$LD_LIBRARY_PATH
  limit vmemoryuse unlimited
  limit

Create a fake Windows installation.
It does not print any error with Fonts... maybe I do not need to load them?

  cd /opt/avalassi/wine0922
  \rm -rf /opt/avalassi/wine0922/win32
  mkdir /opt/avalassi/wine0922/win32
  setenv WINEPREFIX /opt/avalassi/wine0922/win32
  wineprefixcreate

This time it succeeds (status=0), but it still prints a warning.
The wined3d.dll library is missing, most likely because opengl is missing.
I tried setenv WINEDEBUG -wole but it does not help.
I had a look at a recipe, but it uses apt-get and is for Debian:
http://www.winehq.org/pipermail/wine-users/2006-December/023878.html
Let's just ignore it and see if it causes trouble later on...

  Failed to open the service control manager.
  fixme:ole:ITypeInfo_fnRelease destroy child objects
  err:module:import_dll Library wined3d.dll (which is needed by L"c:\\windows\\system32\\d3d8.dll") not found
  /opt/avalassi/wine0922/win32 updated successfully.

Create the appropriate directories.

  cd $WINEPREFIX/dosdevices
  rm -f z:
  ln -s /afs z:
  ln -s / e:
  \rm -rf /tmp/wine0922
  mkdir /tmp/wine0922
  ln -s /tmp/wine0922 t:

I copy the VisualStudio tar files from the previous install:

  cd /opt/avalassi/wine0922
  cp /opt/avalassi/wineDownloads/wineMSVS.tar.gz .
  cp /opt/avalassi/wineDownloads/wineMSVC.tar.gz .
  mkdir -p \
   "$WINEPREFIX/dosdevices/c:/Program Files/Microsoft Visual Studio .NET 2003"
  cp wineMSVS.tar.gz \
   "$WINEPREFIX/dosdevices/c:/Program Files/Microsoft Visual Studio .NET 2003"
  cp wineMSVC.tar.gz \
   "$WINEPREFIX/dosdevices/c:/windows/system32"
  cd \
   "$WINEPREFIX/dosdevices/c:/Program Files/Microsoft Visual Studio .NET 2003"
  tar -xvzf wineMSVS.tar.gz
  \rm wineMSVS.tar.gz
  cd "$WINEPREFIX/dosdevices/c:/windows/system32"
  tar -xvzf wineMSVC.tar.gz
  \rm wineMSVC.tar.gz

Copy the Fonts too? Yes, because the fonts directory is empty at the moment.

  cd /opt/avalassi/wine0922
  cp /opt/avalassi/wineDownloads/wineFonts.tar.gz .
  cp wineFonts.tar.gz \
   "$WINEPREFIX/dosdevices/c:/windows"
  cd "$WINEPREFIX/dosdevices/c:/windows"
  tar -xvzf wineFonts.tar.gz
  \rm wineFonts.tar.gz
  \rm -rf fonts
  mv Fonts fonts

Create a fake directory for fonts.
In previous installations, if I did not do this 
I could get an error message when starting regedit.

  mkdir /opt/avalassi/wine0922/wine-0.9.22-install/share/wine/fonts

Modify the registries. 

  cd /opt/avalassi/wine0922
  \cp /opt/avalassi/wineDownloads/RegistryChanges.reg .
  limit vmemoryuse unlimited
  regedit /e FullRegistryOld.reg
  regedit RegistryChanges.reg
  regedit /e FullRegistryNew.reg
  diff FullRegistryNew.reg FullRegistryOld.reg

----------------------------------
Build COOL on WINE 0.9.22 on SLC4
----------------------------------

* AV 10/10/2007

Add the following to the setupWine script:

  setenv PATH \
    /opt/avalassi/wine0922/wine-0.9.22-install/bin:$PATH
  setenv LD_LIBRARY_PATH \
    /opt/avalassi/wine0922/wine-0.9.22-install/lib:$LD_LIBRARY_PATH
  setenv WINEPREFIX /opt/avalassi/wine0922/win32
  limit vmemoryuse unlimited
  setenv SCRAM_ARCH win32_vc71_dbg_wine

Execute the setup script, then build COOL:

  cd ~/myLCG/COOL_HEAD/src
  scram setup
  scram b

All is ok again.

--------------------------------
Run COOL on WINE 0.9.22 on SLC4
--------------------------------

Initially I got the same problem as under SLC3.
Then I understood it (missing .cache in SEAL and CORAL) and all is ok.
I did not retest WINE 0.9.46 on SLC4 instead.
